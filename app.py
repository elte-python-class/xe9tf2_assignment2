from flask import Flask, jsonify, request
import pymongo


db = pymongo.MongoClient('localhost')["PYTHONCLASS2020"]

app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello, dear user!"

@app.route('/echo')
def echo_():
    return request.args.get("echostring")
    
@app.route('/calc', methods=["POST","GET"])
def cal_():
    if request.method=="GET":
         a = float(request.args.get("a"))
         b = float(request.args.get("b"))
    else:
        req = request.get_json()
        a = float(req["a"])
        b = float(req["b"])
    sum_ = a+b
    div = a/b
    result = {"div": div, "sum": sum_ }
    return result



@app.route('/mongo/names', methods=["POST"])
def names():
    req = request.get_json()
    coll = db["main"]
    found = []
    for d in coll.find({"iteration": int(req["iteration"])}):
        found.append(d["name"])
    result = {"name": found}
    return jsonify(result["name"])


@app.route('/mongo/create_user', methods=["POST"])
def createuser():
    req = request.get_json()
    coll = db["xe9tf2"]
    final = {"username": req["username"], "email": req["email"], "password": req["password"], "name": req["name"]}
    coll.insert_one(final)
    return {"response": "ok"}

@app.route('/mongo/auth/names', methods=["POST"])
def compare():
    req = request.get_json()
    coll = db["xe9tf2"]
    username=[]
    password=[]
    hitsname=[]
    status=[]
    for d in coll.find({"$or":[{"username": req["username"]},{"password": req["password"]}]}):
        username.append(d["username"])
        password.append(d["password"])
    if req["username"] in username and req["password"] in password:
        for e in db["main"].find({"iteration": int(req["iteration"])}):
            hitsname.append(e["name"])
            status=["ok"]
    elif req["username"] not in username:
        status=["bad username"]
    else: status=["bad password"]

    result={"list_of_names": hitsname, "status": status[0]}
    return result

@app.route('/mongo/clear')
def clear():
    coll = db["xe9tf2"]
    coll.delete_many({"username" : "frocli99"})
    return {"response": "ok"}

